// vue.config.js
const webpack = require('webpack') //引入webpack
const path = require('path') // node自带

function resolve(dir) {
	return path.join(__dirname, dir)
}

const {
	defineConfig
} = require('@vue/cli-service')

// 将文件打包压缩成.gz
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const productionGzipExtensions = ['js', 'css']
// // 去掉注释、console插件,开启会导致.map文件不生成
const uglifyJsPlugin = require('uglifyjs-webpack-plugin')

// 代理服务
const ProxyUrl = process.env.VUE_APP_BASE_IP + (process.env.VUE_APP_BASE_PORT ? ':' + process.env.VUE_APP_BASE_PORT :
	'')

// // 开发环境
// if(process.env.NODE_ENV == 'development'){} 
// // 生产环境
// if(process.env.NODE_ENV == 'production'){}
// // 测试环境
// if(process.env.NODE_ENV == 'test'){}

module.exports = defineConfig({
	// 项目根路径
	publicPath: '/',
	// 打包输出目录
	outputDir: 'dist',
	// 静态资源目录
	assetsDir: '', // 'static'
	// 页面入口
	pages: {
		index: {
			// page 的入口
			entry: 'src/main.js',
			// 模板来源
			template: 'public/index.html',
			// 在 dist/index.html 的输出
			filename: 'index.html',
			// 当使用 title 选项时，
			// template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
			title: 'VUE2',
			// 在这个页面中包含的块，默认情况下会包含
			// 提取出来的通用 chunk 和 vendor chunk。
			chunks: ['chunk-vendors', 'chunk-common', 'index']
		},
		// 当使用只有入口的字符串格式时，
		// 模板会被推导为 `public/subpage.html`
		// 并且如果找不到的话，就回退到 `public/index.html`。
		// 输出文件名会被推导为 `subpage.html`。
		// subpage: 'src/subpage/main.js'
	},
	// 保存校验语法
	lintOnSave: false,
	// 转译依赖，开启运行命令时执行比较慢
	transpileDependencies: false,
	// 打包生成环境，是否有map文件
	productionSourceMap: true,

	//  webpack打包的配置
	// configureWebpack: {},
	configureWebpack: function() {
		// 环境判断，开发环境不去掉console和注释
		if (process.env.NODE_ENV != "development") {
			// 去掉注释和console
			new uglifyJsPlugin({
				// uglifyOptions: {
				// 	compress: {
				// 		drop_debugger: true, // 清除debugger语句
				// 		drop_console: true, // 清除console语句
				// 		pure_funcs: ['console.log'], // 生产环境清除console
				// 	}
				// },
				sourceMap: true // 开启.map文件生成
				// parallel: true,
				// cache: true
			})
		};

		// 配置compression-webpack-plugin压缩
		new CompressionWebpackPlugin({
			algorithm: 'gzip',
			test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
			threshold: 10240,
			minRatio: 0.8
		});

		// 限制webpack打出来的chunk.js数量，数量多影响加载速度
		// new webpack.optimize.LimitChunkCountPlugin({
		// 	maxChunks: process.env.NODE_ENV === "development" ? 8 : 3, // 发版压缩成3个js文件，本地压缩成8个js文件
		// 	minChunkSize: 100
		// })
	},
	// chainWebpack: (config) => {
	// 	// 静态资源文件处理
	// 	// 将小于 20kb 的资源内联，以减少 HTTP 请求的数量
	// 	config.module
	// 		.rule('images')
	// 		.use('url-loader')
	// 		.loader('url-loader')
	// 		.tap(options => Object.assign(options, {
	// 			limit: 20480
	// 		}))
	// },
	// 样式
	css: {
		// 开启样式map文件
		sourceMap: true,
		loaderOptions: {
			css: {
				// 这里的选项会传递给 css-loader
			},
			less: {
				lessOptions: {
					javascriptEnabled: true
				}
			}
		}
	},
	// 本地服务
	devServer: {
		open: true, // 在浏览器打开
		// host: '127.0.0.1',
		// port: 3000,
		// https: false,
		// 代理
		proxy: {
			//接口服务
			'/portal': {
				target: ProxyUrl,
				ws: true,
				changeOrigin: true,
				logLevel: 'debug'
			},
			// //接口服务
			// '/platform': {
			// 	target: plProxyUrl,
			// 	ws: true,
			// 	changeOrigin: true,
			// 	logLevel: 'debug'
			// }
		}
	},
	// 第三方插件
	pluginOptions: {
		'style-resources-loader': {
			preProcessor: 'less',
			patterns: [
				path.resolve(__dirname, './src/assets/*.less') // less所在文件路径
			]
		}
	}
})
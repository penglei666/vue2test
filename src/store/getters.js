const getters = {
	// app
	drive: state => state.app.drive, //开启引导页
	collapse: state => state.app.collapse, //菜单收起
	
	// user
	userId: state => state.user.userId, //userId
	userCode: state => state.user.userCode, //userCode
	name: state => state.user.name, //用户名
	gender: state => state.user.gender, // 性别 (1:男，2：女)
	userDept: state => state.user.userDept, // 用户所在局
	userDeptId: state => state.user.userDeptId, // 用户所在局id
	userDeptList: state => state.user.userDeptList, // 用户所属机构（层级）
	myDeptName: state => state.user.myDeptName, //用户处室
	introduction: state => state.user.introduction, //描述
	roles: state => state.user.roles, //用户角色
	info: state => state.user.info, // 当前登录用户信息
	menus: state => state.user.menus, //当前用户菜单
}
export default getters

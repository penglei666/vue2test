//引入vue
import Vue from 'vue'
import {
	login,
	getInfo
} from '@/api/user.js'

// 状态
const state = {
	userId: '', //userId
	userCode: '', //userCode
	name: '', //用户名
	gender: '', // 性别 (1:男，2：女)
	userDept: '', // 用户所在局
	userDeptId: '', // 用户所在局id
	userDeptList: [], // 用户所属机构（层级）
	myDeptName: '', //用户处室
	introduction: '', //描述
	roles: [], //用户角色
	isAdmin: false, //是否是管理员，默认否
	isSuperAdmin: false, //是否是超级管理员，默认否
	hasPage: false, //是否有自定义页面，默认无
	loginCount: 3, // 当前用户登录次数
	isShowReport: 1, // 是否显示当前用户年度统计报告
	info: {}, // 当前登录用户信息
	//当前用户菜单
	menus: [{
			id: 0,
			pid: 'root',
			title: '首页',
			path: '/home',
			name: 'home',
			show: true,
			icon: 'el-icon-s-home'
		},
		{
			id: 1,
			pid: 'root',
			title: '开发用例',
			path: '/example',
			name: 'example',
			show: true,
			icon: 'el-icon-menu',
			redirect: '/example/icon/list',
		},
		{
			id: 11,
			pid: 1,
			title: '图标',
			path: '/example/icon',
			name: 'icon',
			show: true,
			icon: ''
		},
		{
			id: 111,
			pid: 11,
			title: '列表页',
			path: '/example/icon/list',
			name: 'iconList',
			show: false,
			icon: ''
		},
		{
			id: 112,
			pid: 11,
			title: '详情页',
			path: '/example/icon/detail',
			name: 'iconDetail',
			show: false,
			icon: ''
		},
		{
			id: 113,
			pid: 11,
			title: '编辑页',
			path: '/example/icon/edit',
			name: 'iconEdit',
			show: false,
			icon: ''
		},
		{
			id: 12,
			pid: 1,
			title: '表格',
			path: '/example/grid',
			name: 'grid',
			show: true,
			icon: ''
		},
		{
			id: 121,
			pid: 12,
			title: '列表页',
			path: '/example/grid/list',
			name: 'gridList',
			show: false,
			icon: ''
		},
		{
			id: 122,
			pid: 12,
			title: '详情页',
			path: '/example/grid/detail',
			name: 'gridDetail',
			show: false,
			icon: ''
		},
		{
			id: 123,
			pid: 12,
			title: '编辑页',
			path: '/example/grid/edit',
			name: 'gridEdit',
			show: false,
			icon: ''
		},
		{
			id: 13,
			pid: 1,
			title: '列表',
			path: '/example/list',
			name: 'list',
			show: true,
			icon: ''
		},
		{
			id: 131,
			pid: 13,
			title: '列表页',
			path: '/example/list/list',
			name: 'listList',
			show: false,
			icon: ''
		},
		{
			id: 132,
			pid: 13,
			title: '详情页',
			path: '/example/list/detail',
			name: 'listDetail',
			show: false,
			icon: ''
		},
		{
			id: 133,
			pid: 13,
			title: '编辑页',
			path: '/example/list/edit',
			name: 'listEdit',
			show: false,
			icon: ''
		},
		{
			id: 2,
			pid: 'root',
			title: '系统管理',
			path: '/sys',
			show: true,
			icon: 'el-icon-s-tools',
			redirect: '/sys/user/list',
		},
		{
			id: 25,
			pid: 2,
			title: '数据来源管理',
			path: '/sys/datasource',
			name: 'datasource',
			show: true,
			icon: ''
		},
		{
			id: 251,
			pid: 25,
			title: '列表页',
			path: '/sys/datasource/list',
			name: 'datasourceList',
			show: false,
			icon: ''
		},
		{
			id: 252,
			pid: 25,
			title: '编辑页',
			path: '/sys/datasource/Edit',
			name: 'datasourceEdit',
			show: false,
			icon: ''
		},
		{
			id: 26,
			pid: 2,
			title: '参数配置',
			path: '/sys/dist',
			name: 'dist',
			show: true,
			icon: ''
		},
		{
			id: 261,
			pid: 26,
			title: '列表页',
			path: '/sys/dist/list',
			name: 'distList',
			show: false,
			icon: ''
		},
		{
			id: 21,
			pid: 2,
			title: '用户管理',
			path: '/sys/user',
			name: 'user',
			show: true,
			icon: ''
		},
		{
			id: 211,
			pid: 21,
			title: '列表页',
			path: '/sys/user/list',
			name: 'userList',
			show: false,
			icon: ''
		},
		{
			id: 212,
			pid: 21,
			title: '详情页',
			path: '/sys/user/detail',
			name: 'userDetail',
			show: false,
			icon: ''
		},
		{
			id: 213,
			pid: 21,
			title: '编辑页',
			path: '/sys/user/edit',
			name: 'userEdit',
			show: false,
			icon: ''
		},
		{
			id: 22,
			pid: 2,
			title: '角色管理',
			path: '/sys/role',
			name: 'role',
			show: true,
			icon: ''
		},
		{
			id: 221,
			pid: 22,
			title: '列表页',
			path: '/sys/role/list',
			name: 'roleList',
			show: false,
			icon: ''
		},
		{
			id: 222,
			pid: 22,
			title: '详情页',
			path: '/sys/role/detail',
			name: 'roleDetail',
			show: false,
			icon: ''
		},
		{
			id: 223,
			pid: 22,
			title: '编辑页',
			path: '/sys/role/edit',
			name: 'roleEdit',
			show: false,
			icon: ''
		},
		{
			id: 23,
			pid: 2,
			title: '部门管理',
			path: '/sys/dept',
			name: 'dept',
			show: true,
			icon: ''
		},
		{
			id: 231,
			pid: 23,
			title: '列表页',
			path: '/sys/dept/list',
			name: 'deptList',
			show: false,
			icon: ''
		},
		{
			id: 232,
			pid: 23,
			title: '详情页',
			path: '/sys/dept/detail',
			name: 'deptDetail',
			show: false,
			icon: ''
		},
		{
			id: 233,
			pid: 23,
			title: '编辑页',
			path: '/sys/dept/edit',
			name: 'deptEdit',
			show: false,
			icon: ''
		},
		{
			id: 24,
			pid: 2,
			title: '岗位管理',
			path: '/sys/job',
			name: 'job',
			show: true,
			icon: ''
		},
		{
			id: 241,
			pid: 24,
			title: '列表页',
			path: '/sys/job/list',
			name: 'jobList',
			show: false,
			icon: ''
		},
		{
			id: 242,
			pid: 24,
			title: '详情页',
			path: '/sys/job/detail',
			name: 'jobDetail',
			show: false,
			icon: ''
		},
		{
			id: 243,
			pid: 24,
			title: '编辑页',
			path: '/sys/job/edit',
			name: 'jobEdit',
			show: false,
			icon: ''
		},
	]
}

// 通过提交mutations的方式修改state中的变量的值
// 全局调用： this.$store.commit('app/SetDrive', true)
const mutations = {
	SET_TOKEN: (state, token) => {
		state.token = token
	},
	SET_USERID: (state, userId) => {
		state.userId = userId
	},
	SET_USER_CODE: (state, userCode) => {
		state.userCode = userCode
	},
	SET_DEPTNAME: (state, deptName) => {
		state.userDept = deptName
	},
	SET_DEPTNAMEID: (state, deptId) => {
		state.userDeptId = deptId
	},
	SET_USERDEPTLIST: (state, depts) => {
		state.userDeptList = depts
	},
	SET_NO_SHOW_NOTIFY: (state, value) => {
		state.noShowNotify = value
	},
	SET_MYDEPTNAME: (state, myDeptName) => {
		state.myDeptName = myDeptName
	},
	SET_NAME: (state, name) => {
		state.name = name
	},
	// 性别
	SET_GENDER: (state, gender) => {
		state.gender = gender
	},
	//简介
	SET_INTRODUCTION: (state, introduction) => {
		state.introduction = introduction
	},
	//角色
	SET_ROLES: (state, roles) => {
		state.roles = roles
	},
	//是否管理员
	SET_ISADMIN: (state, value) => {
		state.isAdmin = value
	},
	//是否超级管理员
	SET_ISSUPERADMIN: (state, value) => {
		state.isSuperAdmin = value
	},
	//是否有自定义页面
	SET_HASPAGE: (state, value) => {
		state.hasPage = value
	},
	//当前用户登录次数
	SET_LOGIN_COUNT: (state, value) => {
		state.loginCount = value
	},
	//是否显示当前用户年度统计报告
	SET_IS_SHOW_REPORT: (state, value) => {
		state.isShowReport = value
	},
	//当前登录用户信息
	SET_INFO: (state, value) => {
		state.info = value
	}
}

// 异步的，可以调用接口后，再修改全局状态
const actions = {
	// user login
	_login({
		commit
	}, userInfo) {
		const {
			username,
			password
		} = userInfo
		return new Promise((resolve, reject) => {
			login({
				username: username.trim(),
				password: password
			}).then(response => {
				const {
					data
				} = response //对象解构： { data:data }
				resolve(data)
			}).catch(error => {
				reject(error)
			})
		})
	},

	// get user info
	_getInfo({
		commit,
		state,
		dispatch
	}) {
		return new Promise((resolve, reject) => {
			getInfo().then(response => {
				const {
					data
				} = response

				if (!data) {
					reject('验证失败，请重新登录！')
				}

				const {
					userId,
					userCode,
					depts,
					userName,
					hasPage,
					sex,
					isShowReport
				} = data

				const myDeptName = depts[0].codeName // 获取当前部门
				let deptName = '' // 获取父级部门
				let deptNameId = '' // 获取局级部门id

				// 向上找部门等级不为3的父级部门简称
				for (let i = 1; i < depts.length; i++) {
					if (depts[i].deptLevel != 3) {
						deptName = depts[i].codeName
						deptNameId = depts[i].uuid
						break
					}
				}

				// 把当前部门列表存在数组
				let userDeptLists = []
				for (let i = 0; i < depts.length; i++) {
					userDeptLists.push(depts[i].unitCode || depts[i].uuid)
				}

				commit('SET_USERID', userId)
				commit('SET_USER_CODE', userCode)
				commit('SET_DEPTNAME', deptName)
				commit('SET_DEPTNAMEID', deptNameId)
				commit('SET_USERDEPTLIST', userDeptLists)
				commit('SET_MYDEPTNAME', myDeptName)
				commit('SET_NAME', userName)
				commit('SET_GENDER', sex)
				commit('SET_HASPAGE', hasPage)
				commit('SET_IS_SHOW_REPORT', isShowReport)
				commit('SET_INFO', data)

				resolve(data)
			}).catch(error => {
				reject(error)
			})
		})
	},
}

// 抛出的属性
export default {
	//命名空间，若为true，则使用vuex的时候需要加上"模块名/"
	namespaced: true,
	state,
	mutations,
	actions
}

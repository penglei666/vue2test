//引入vue
import Vue from 'vue'

// 状态
const state = {
	drive: true, //开启引导页
	collapse: false // 菜单收起
}

// 通过提交mutations的方式修改state中的变量的值
// 全局调用： this.$store.commit('app/SetDrive', true)
const mutations = {
	//设置开启引导页
	Set_Drive: (state, status) => {
		state.drive = status
	},
	//设置菜单折叠
	Set_Collapse: (state, status) => {
		state.collapse = status
	}
}

// 异步的，可以调用接口后，再修改全局状态
const actions = {
	//设置开启引导页
	setDrive({ commit }, status) {
		commit('Set_Drive', status) // 提交一个mutations去修改值
	}
}

// 抛出的属性
export default {
	//命名空间，若为true，则使用vuex的时候需要加上"模块名/"
	namespaced: true,
	state,
	mutations,
	actions
}
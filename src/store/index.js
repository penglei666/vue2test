import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'

Vue.use(Vuex)

// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = require.context('./modules', true, /\.js$/)

// 不需要手动引入 `import app from './modules/app'`
// 将自动引入vuex模块文件
//keys方法返回属性名，reduce方法累加、累计计算
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
	// set './app.js' => 'app'
	const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
	const value = modulesFiles(modulePath)
	modules[moduleName] = value.default
	return modules
}, {})

const store = new Vuex.Store({
	// strict: true, //开启严格模式，那么任何修改state的操作，只要不经过 mutation的函数，vue就会 throw error
	modules,
	getters
})

export default store


// 两种分文件的方式
// 1.根据vuex属性去区分
/* 
 State
 Getter
 Mutation
 Action
 */

// 2.根据业务功能区分(Module)
// 根据主要的业务功能创建模块js文件
// 每个js里是完整的 store
import Vue from 'vue'
import App from './App.vue'
// 引入element
import './lib/element.js'
// 引入基础样式
import './assets/index.less'

//引入vue路由
import router from './router/index.js'
//引入vuex
import store from './store/index.js'
// 路由拦截及初始信息加载
import './permission'

// 引入axios和VueAxiosss
import axios from './lib/request.js'
// import VueAxios from 'vue-axios'
// 顺序不要错， 同： $.extend({}, {}) 合并
// Vue.use(VueAxios, axios)
// 绑定到原型上
Vue.prototype.$http = axios

import utils from './lib/utils.js'
// 绑定到原型上
Vue.prototype.$utils = utils
// Vue.use(utils)

// 引入并使用自定义指令
import { preventReClick } from './lib/directive.js'
Vue.use(preventReClick)

// 引入组件
import Tab from "./components/tab/index.vue"
// 全局组件注册
Vue.component('Tab',Tab)

Vue.config.productionTip = false

// this-->vue
new Vue({
	router,
	store,
  render: h => h(App),
}).$mount('#app')

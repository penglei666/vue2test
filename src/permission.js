//引入路由
import router from './router'
//引入vuex
import store from './store'
//使用loading组件
// import loading from '@/lib/loading'
//引入路由访问日志接口
// import {
// 	addRouteLogApi, //添加路由访问日志
// } from "@/api/app"
//引入element消息提示工具
import {
	MessageBox
} from 'element-ui'

//路由拦截,当进入一个页面是会触发导航守卫 router.beforeEach 事件
router.beforeEach((to, from, next) => {
	//打开loading
	// loading.router ? loading.show() : null
	// 若未登陆者先登陆
	if (!store.getters.userId) {
		// 获取用户
		// getUserInfo(to, from, next)
		next()
	}
	else {
		next()
	}
	//关闭loading
	// loading.router ? loading.close() : null
})

//路由跳转后触发
router.afterEach((to, from) => {
	if (to.name) {
		// // 添加路由访问日志
		// addRouteLogApi({
		// 	menuRoute: to.name || "", // 路由
		// 	orgId: store.getters.userDeptId||"" // 当前用户所在局id
		// }).then(res => {
		// 	// console.log('路由日志记录')
		// })
	}
})

// 获取用户信息
function getUserInfo(to, from, next) {
	//获取用户信息
	if (!store.getters.userId) {
		// 获取用户信息
		store.dispatch("user/_getInfo").then((res) => {
			if (res && res.userId) {
				// 连接websocket
				// socket.connect()
			}
			next() //放行
			
			// 获取用户其它信息
			// store.dispatch("user/getOther")
		}).catch((error) => {
			next() //放行
		})
	}
}

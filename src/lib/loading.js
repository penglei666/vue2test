/**
 * loading 加载效果封装
 * create by penglei on 2021.03.09
 * */

import { Loading } from 'element-ui'

//全局页面跳转是否启用loading
export const routerLoading = true

//全局api接口调用是否启用loading
export const apiLoading = true

var loading = null
// 初始化打开loading
const show = (option) => {
	//loading参数配置
	let loadingConfig = {
		lock: true, //同 v-loading 指令中的 lock 修饰符（false）
		text: 'Loading', //显示在加载图标下方的加载文案
		spinner: 'el-icon-loading', //自定义加载图标类名
		background: 'rgba(0, 0, 0, 0.7)', //遮罩背景色
		// customClass:"", //Loading 的自定义类名
		// fullscreen: true, //全屏Loading
	}
	//合并参数
	Object.assign(loadingConfig, option ? option : {})
	loading = Loading.service(loadingConfig)
}

//关闭loading
const close = () => {
	loading.close()
}

export default {
	router: routerLoading, //路由loading
	api: apiLoading, //接口loading
	show, //打开
	close //关闭
}

//引入vue
import Vue from 'vue'
//引入axios
import axios from 'axios';

//引入element消息提示工具
// import {
// 	MessageBox,
// 	Message
// } from 'element-ui'
//引入状态管理
import store from '@/store'
//引入路由
import router from '@/router'
//引入公用方法
import utils from './utils'
//引入loading
// import loading from './loading'

//创建axios实例
const service = axios.create({
	// url = base url + request url
	baseURL: process.env.VUE_APP_BASE_API,
	// withCredentials: true, //跨域请求时发送Cookie
	timeout: 30000 //请求超时间
})

// 请求拦截器
service.interceptors.request.use(request => {
		const hasLoading = request.headers.hasOwnProperty("loading")
		/* //开启loading
		if (loading.api && (hasLoading ? request.headers.loading : true)) {
			loading.show()
		} */
		// 单点登录身份认证处理
		let cssSsoTicket = utils.GetUrlParametes('cssSsoTicket')
		let ticket = utils.GetCookie('ticket') || ''
		if (cssSsoTicket) {
			utils.SetCookie('ticket', cssSsoTicket)
			if (request.method) {
				if (request.method.toLocaleLowerCase() == "get") {
					if (request.url.indexOf("?") > -1) {
						request.url += '&cssSsoTicket=' + encodeURI(cssSsoTicket) //使用urlencoder进行中文的十六进制转码
					} else {
						request.url += '?cssSsoTicket=' + encodeURI(cssSsoTicket) //使用urlencoder进行中文的十六进制转码
					}
				}
			}
		} else if (ticket) { // 单点登录身份认证，若存在则加到接口请求的参数中
			if (request.method) {
				if (request.method.toLocaleLowerCase() == "get") {
					if (request.url.indexOf("?") > -1) {
						request.url += '&cssSsoTicket=' + encodeURI(ticket)
					} else {
						request.url += '?cssSsoTicket=' + encodeURI(ticket)
					}
				}
			}
		}
		// 不要忘记
		return request
	},
	error => {
		// 处理请求错误
		console.log(error)
		return Promise.reject(error)
	}
)

// 响应拦截器
service.interceptors.response.use(
	/**
	 * 如果您想获取http信息，如头或状态请返回: response => response
	 * 通过自定义代码确定请求状态，还可以通过HTTP状态代码来判断状态
	 */
	response => {
		//关闭loading
		// if (loading.api) {
		// 	loading.close()
		// }
		const res = response.data
		const contentType = response.headers["content-type"]
		//判断是否登录
		const isLogin = contentType ? contentType.indexOf("text/html") : -1
		if (isLogin != -1 || (res && res.code == "401") || (!response.data && response.status == "401")) {
			//跳转管理端登录页面
			// window.location.href = location.origin + process.env.VUE_APP_BASE_API + "/platform"
			// 当前应用
			let currUrl = location.origin + (process.env.VUE_APP_BASE_ROUTER || '')
			// if (location.href.indexOf('cssSsoTicket=') == -1) {
			let strHref = process.env.VUE_APP_BASE_SSO + '/ssoCheck.action?toUrl=' + currUrl
			// 跳转登录页
			window.location.href = strHref
			utils.SetCookie('ticket', "")
			// }
		}
		return res;
	},
	error => {
		console.log('接口响应失败：' + error)

		//关闭loading
		// if (loading.api) {
		// 	loading.close()
		// }
		return Promise.reject(error)
	}
)

//导出直接使用
export default service

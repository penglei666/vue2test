/* 
 * 自定义指令
 * Created by Penglei
 * On 2023/02/22
 */
import Vue from 'vue';

// 按钮事件间隔点击时间（ms）
const btnClickTime = 1500;

//  v-preventReClick：按钮事件间隔点击时间控制
const preventReClick = Vue.directive('preventReClick', {
	inserted: function(el, binding, vNode, oldVnode) {
		let timer;
		el.addEventListener('click', (e) => {
			// console.log("disabled: true");
			if (!el.disabled) {
				el.disabled = true;
				if (timer) {
				  clearTimeout(timer);
				}
				timer = setTimeout(() => {
					el.disabled = false;
					// console.log("disabled: false");
				}, binding.value || btnClickTime)
			}
		})
	}
})

export {
	preventReClick
}

/* 
	当前应用基础接口
	Create by penglei on 2023/03/16
 */

// 引入ajax
import request from '@/lib/request'

// 获取数据来源
export function fetchDataSourceApi(params) {
	return request({
		url: '/portal/subsystem',
		method: 'get',
		params: params,
		// 参数结构
		// params: {
		// 	name: '',
		// 	page: 1,
		// 	rows: 10
		// }
	})
}
// 通过Id获取当前行数据来源
export function fetchDataSourceByIdApi(id) {
	return request({
		url: '/portal/subsystem/' + id,
		method: 'get'
	})
}
// 添加数据来源
export function addDataSourceApi(data) {
	return request({
		url: '/portal/subsystem',
		method: 'post',
		data: data,
		// data
	})
}
// 修改数据来源
export function editDataSourceApi(data) {
	return request({
		url: '/portal/subsystem',
		method: 'put',
		data,
	})
}
// 删除数据来源
export function delDataSourceApi(data) {
	return request({
		url: '/portal/subsystem',
		method: 'delete',
		data,
	})
}


// 获取参数配置
export function fetchDictApi(params) {
	return request({
		url: '/portal/dicts/queryDicts',
		method: 'get',
		params: params,
		// // 参数结构
		// params: {
		// 	dictName: '',
		// 	dictCode: '',
		// 	page: 1,
		// 	rows: 10
		// }
	})
}
// 验证编码是否重复
export function validateDictCodeApi(params) {
	return request({
		url: '/portal/dicts/validateDictCode',
		method: 'get',
		params,
		// 参数结构
		// params: {
		// 	dictId: '',
		// 	dictCode: ''
		// }
	})
}
// 通过Id获取当前行参数配置
export function fetchDictByIdApi(id) {
	return request({
		url: '/portal/dicts/' + id,
		method: 'get'
	})
}
// 添加参数配置
export function addDictApi(data) {
	return request({
		url: '/portal/dicts',
		method: 'post',
		data: data,
		// data
	})
}
// 修改参数配置
export function editDictApi(data) {
	return request({
		url: '/portal/dicts',
		method: 'put',
		data,
	})
}
// 删除参数配置
export function delDictApi(ids) {
	return request({
		url: '/portal/dicts/' + ids,
		method: 'delete'
	})
}
/* 
	当前应用基础接口
	Create by penglei on 2021.04.01
 */

// 引入ajax
import request from '@/lib/request'

// 获取数据字典项
export function fetchDictItemApi(params) {
	return request({
		url: '/portal/api/commons/dictitembydictcode',
		method: 'get',
		params,
		// 参数结构
		// params: {
		// 	dictCodes: "resource_type", //获取资源类型字典项
		// }
	})
}

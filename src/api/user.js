import request from '@/lib/request'

// 登录
export function login(data) {
	return request({
		// url: '/platform/api/org/users/login',
		url: '/portal/api/users/login',
		method: 'post',
		data
	})
}

// 获取用户信息
export function getInfo() {
	return request({
		url: '/portal/user/userInfo/curUser',
		method: 'get',
		// // `headers` 是即将被发送的自定义请求头
		// headers: {
		// 	'loading': false  //当前接口是否开启加载动画
		// },
	})
}

// 退出
export function logout() {
	return request({
		url: '/portal/loginOut',
		method: 'get'
	})
}

// 展示端退出
export function webLogout() {
	return request({
		url: '/portal/webLoginOut',
		method: 'get'
	})
}

// 是否是管理员
export function getIsAdmin() {
	return request({
		url: '/portal/user/userInfo/isAdmin',
		method: 'get'
	})
}

// 是否是超级管理员
export function getIsSuperAdmin() {
	return request({
		url: '/portal/user/userInfo/isSuperAdmin',
		method: 'get'
	})
}

// 获取当前用户未读邮件数
export function getEmailApi(attr) {
	return request({
		url: '/portal/mail/getAttrs?attr=' + attr, // mbox_newmsgcnt
		method: 'get'
	})
}

// 获取当前用户所在机构的公文、电报路由
export function getTodoWorkStatisticsRoute() {
	return request({
		url: '/portal/oabase/api/getTodoWorkStatisticsRoute',
		method: 'get'
	})
}

// 当前用户个人年度统计报告设为已查看
export function setReadReportApi() {
	return request({
		url: '/portal/user/userInfo/readReport ',
		method: 'put'
	})
}

// 系统更新日报
export function getSystemLogsByUser(params) {
	return request({
		url: '/portal/logs/querylogsbyuser',
		method: 'get',
		params
	})
}
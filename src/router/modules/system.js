// 系统管理路由
// 懒加载 require.ensure

// 框架
const Layout = resolve => {
	require.ensure(['@/layout/index.vue'], () => {
		resolve(require('@/layout/index.vue'))
	})
}
// 用户管理
const User = resolve => {
	require.ensure(['@/views/sys/user/index.vue'], () => {
		resolve(require('@/views/sys/user/index.vue'))
	})
}
const UserDetail = resolve => {
	require.ensure(['@/views/sys/user/detail.vue'], () => {
		resolve(require('@/views/sys/user/detail.vue'))
	})
}
const UserEdit = resolve => {
	require.ensure(['@/views/sys/user/edit.vue'], () => {
		resolve(require('@/views/sys/user/edit.vue'))
	})
}

// 角色管理
const Role = resolve => {
	require.ensure(['@/views/sys/role/index.vue'], () => {
		resolve(require('@/views/sys/role/index.vue'))
	})
}

// 部门管理
const Dept = resolve => {
	require.ensure(['@/views/sys/dept/index.vue'], () => {
		resolve(require('@/views/sys/dept/index.vue'))
	})
}

// 岗位管理
const Job = resolve => {
	require.ensure(['@/views/sys/job/index.vue'], () => {
		resolve(require('@/views/sys/job/index.vue'))
	})
}

export default [{
		// 默认跳转第一个
		name: 'sys',
		path: '/sys',
		redirect: '/sys/datasource/list',
		meta: {
			title: '系统管理'
		}
	},
	// 用户
	{
		name: 'user',
		path: '/sys/user',
		redirect: '/sys/user/list',
		component: Layout,
		meta: {
			title: '用户',
			keepAlive: true //需要被缓存的组件
		},
		children: [{
				// pageNo:分页参数
				name: 'userList',
				path: '/sys/user/list:pageNo([1-9]\\d*)?',
				component: User,
				meta: {
					title: '列表页',
					keepAlive: true //需要被缓存的组件
				}
			},
			{
				// 动态路由，可以传递参数, ?:参数可传可不传,跟在最后边
				name: 'userDetail',
				path: '/sys/user/detail/:id?',
				component: UserDetail,
				meta: {
					title: '详情页',
					keepAlive: true //需要被缓存的组件
				}
			},
			{
				name: 'userEdit',
				path: '/sys/user/edit',
				component: UserEdit,
				meta: {
					title: '编辑页',
					keepAlive: true //需要被缓存的组件
				}
			}
		]
	},
	// 角色
	{
		name: 'role',
		path: '/sys/role',
		redirect: '/sys/role/list',
		component: Layout,
		meta: {
			title: '角色',
			keepAlive: true //需要被缓存的组件
		},
		children: [{
			name: 'roleList',
			path: '/sys/role/list',
			component: Role,
			name: 'role',
			meta: {
				title: '列表页',
				keepAlive: true //需要被缓存的组件
			}
		}]
	},
	// 部门
	{
		name: 'dept',
		path: '/sys/dept',
		redirect: '/sys/dept/list',
		component: Layout,
		meta: {
			title: '部门',
			keepAlive: true //需要被缓存的组件
		},
		children: [{
			name: 'deptList',
			path: '/sys/dept/list',
			component: Dept,
			meta: {
				title: '列表页',
				keepAlive: true //需要被缓存的组件
			}
		}]
	},
	// 岗位
	{
		name: 'job',
		path: '/sys/job',
		redirect: '/sys/job/list',
		component: Layout,
		meta: {
			title: '岗位',
			keepAlive: true //需要被缓存的组件
		},
		children: [{
			name: 'jobList',
			path: '/sys/job/list',
			component: Job,
			meta: {
				title: '列表页',
				keepAlive: true //需要被缓存的组件
			}
		}]
	},
	// 数据来源管理
	{
		name: 'datasource',
		path: '/sys/datasource',
		redirect: '/sys/datasource/list',
		component: Layout,
		meta: {
			title: '数据来源管理',
			keepAlive: true //需要被缓存的组件
		},
		children: [{
				name: 'datasourceList',
				path: '/sys/datasource/list',
				// 引入文件
				component: resolve => {
					require.ensure(['@/views/sys/datasource'], () => {
						resolve(require('@/views/sys/datasource'))
					})
				},
				meta: {
					title: '列表页',
					keepAlive: true //需要被缓存的组件
				}
			},
			{
				name: 'datasourceEdit',
				path: '/sys/datasource/edit/:id?', // ?编辑和添加通用一个页面
				// 引入文件
				component: resolve => {
					require.ensure(['@/views/sys/datasource/edit.vue'], () => {
						resolve(require('@/views/sys/datasource/edit.vue'))
					})
				},
				meta: {
					title: '编辑页',
					keepAlive: true //需要被缓存的组件
				}
			}
		]
	},
	// 参数配置
	{
		name: 'dist',
		path: '/sys/dist',
		redirect: '/sys/dist/list',
		component: Layout,
		meta: {
			title: '参数配置',
			keepAlive: true //需要被缓存的组件
		},
		children: [{
			name: 'dist',
			path: '/sys/dist/list',
			// 引入文件
			component: resolve => {
				require.ensure(['@/views/sys/dist'], () => {
					resolve(require('@/views/sys/dist'))
				})
			},
			meta: {
				title: '列表页',
				keepAlive: true //需要被缓存的组件
			}
		}]
	}
]

// 开发用例管理路由
// 懒加载 require.ensure

// 框架
const Layout = resolve => {
	require.ensure(['@/layout/index.vue'], () => {
		resolve(require('@/layout/index.vue'))
	})
}
// 图标管理
const Icon = resolve => {
	require.ensure(['@/views/example/icon/index.vue'], () => {
		resolve(require('@/views/example/icon/index.vue'))
	})
}
const IconDetail = resolve => {
	require.ensure(['@/views/example/icon/detail.vue'], () => {
		resolve(require('@/views/example/icon/detail.vue'))
	})
}
const IconEdit = resolve => {
	require.ensure(['@/views/example/icon/edit.vue'], () => {
		resolve(require('@/views/example/icon/edit.vue'))
	})
}

// 表格管理
const Grid = resolve => {
	require.ensure(['@/views/example/grid/index.vue'], () => {
		resolve(require('@/views/example/grid/index.vue'))
	})
}
const GridDetail = resolve => {
	require.ensure(['@/views/example/grid/detail.vue'], () => {
		resolve(require('@/views/example/grid/detail.vue'))
	})
}
const GridEdit = resolve => {
	require.ensure(['@/views/example/grid/edit.vue'], () => {
		resolve(require('@/views/example/grid/edit.vue'))
	})
}

// 列表管理
const List = resolve => {
	require.ensure(['@/views/example/list/index.vue'], () => {
		resolve(require('@/views/example/list/index.vue'))
	})
}
const ListDetail = resolve => {
	require.ensure(['@/views/example/list/detail.vue'], () => {
		resolve(require('@/views/example/list/detail.vue'))
	})
}
const ListEdit = resolve => {
	require.ensure(['@/views/example/list/edit.vue'], () => {
		resolve(require('@/views/example/list/edit.vue'))
	})
}

export default [{
		// 默认跳转第一个
		name: 'example',
		path: '/example',
		redirect: '/example/icon/list',
		meta: {
			title: '开发用例',
		}
	},
	// 图标
	{
		name: 'icon',
		path: '/example/icon',
		redirect: '/example/icon/list',
		component: Layout,
		meta: {
			title: '图标',
			keepAlive: true //需要被缓存的组件
		},
		children: [{
				name: 'iconList',
				path: '/example/icon/list',
				component: Icon,
				meta: {
					title: '列表页',
					keepAlive: true //需要被缓存的组件
				}
			},
			{
				name: 'iconDetail',
				path: '/example/icon/detail',
				component: IconDetail,
				meta: {
					title: '详情页',
					keepAlive: true //需要被缓存的组件
				}
			},
			{
				name: 'iconEdit',
				path: '/example/icon/edit',
				component: IconEdit,
				meta: {
					title: '编辑页',
					keepAlive: true //需要被缓存的组件
				}
			}
		]
	},
	// 表格
	{
		name: 'grid',
		path: '/example/grid',
		redirect: '/example/grid/list',
		component: Layout,
		meta: {
			title: '表格',
			keepAlive: true //需要被缓存的组件
		},
		children: [{
				name: 'gridList',
				path: '/example/grid/list',
				component: Grid,
				meta: {
					title: '列表页',
					keepAlive: true //需要被缓存的组件
				}
			},
			{
				// 动态路由配置:id-->动态参数
				// ?:参数可有可无
				name: 'gridDetail',
				path: '/example/grid/detail',
				component: GridDetail,
				meta: {
					title: '详情页',
					keepAlive: true //需要被缓存的组件
				}
			},
			{
				name: 'gridEdit',
				path: '/example/grid/edit',
				component: GridEdit,
				meta: {
					title: '编辑页',
					keepAlive: true //需要被缓存的组件
				}
			}
		]
	},
	// 列表
	{
		name: 'list',
		title: '列表',
		path: '/example/list',
		redirect: '/example/list/list',
		component: Layout,
		children: [{
				name: 'listList',
				title: '列表页',
				path: '/example/list/list',
				component: List,
				meta: {
					keepAlive: true //需要被缓存的组件
				}
			},
			{
				// 动态路由配置:id-->动态参数
				// ?:参数可有可无
				name: 'listDetail',
				title: '详情页',
				path: '/example/list/detail',
				component: ListDetail,
				meta: {
					keepAlive: true //需要被缓存的组件
				}
			},
			{
				name: 'listEdit',
				title: '编辑页',
				path: '/example/list/edit',
				component: ListEdit,
				meta: {
					keepAlive: true //需要被缓存的组件
				}
			}
		]
	}
]

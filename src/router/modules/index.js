// 引入开发用例路由
import example from './example'
// 引入系统管理路由
import system from './system'

// 框架
const Layout = resolve => {
	require.ensure(['@/layout/index.vue'], () => {
		resolve(require('@/layout/index.vue'))
	})
}
// 默认首页
const Home = resolve => {
	require.ensure(['@/views/home.vue'], () => {
		resolve(require('@/views/home.vue'))
	})
}
// 错误页
const Error = resolve => {
	require.ensure(['@/views/error.vue'], () => {
		resolve(require('@/views/error.vue'))
	})
}

export default [{
		// 默认
		path: '/',
		redirect: '/home',
	},
	{
		path: '/home',
		component: Layout,
		children: [{
			name: 'home',
			path: '/home',
			component: Home,
			meta: {
				title: '首页',
				keepAlive: true //需要被缓存的组件
			},
		}]
	},
	{
		path: '/404',
		name: 'error',
		component: Error,
		hidden: true
	},
	// 路由错误默认跳转首页
	{
		path: '*',
		redirect: '/home',
		hidden: true
	},
	// 开发用例路由
	...example,
	// 系统管理路由
	...system
]

//引入vue
import Vue from 'vue'
// 引入vue-router
import VueRouter from 'vue-router'

// 引入基础路由
import index from './modules/index'


// 解决路由经常报错NavigationDuplicated问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
	return originalPush.call(this, location).catch(err => err)
}

// 使用vue-router
Vue.use(VueRouter)

// 2. 定义路由
// 每个路由应该映射一个组件。 其中"component" 可以是
// 通过 Vue.extend() 创建的组件构造器，
// 或者，只是一个组件配置对象。
// 我们晚点再讨论嵌套路由。
const routes = [
	// 基础路由
	...index
]

// 3. 创建 router 实例，然后传 `routes` 配置
// 你还可以传别的配置参数, 不过先这么简单着吧。
const createRouter = () => new VueRouter({
	mode: 'hash', //hash、history
	// base: '/', // process.env.VUE_APP_BASE_ROUTER
	routes // (缩写) 相当于 routes: routes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
	const newRouter = createRouter()
	router.matcher = newRouter.matcher //重置路由
}

export default router
